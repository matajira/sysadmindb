
import sys
sys.path.append('../src')
import unittest
from sysadmindb import log

class TestMessage(unittest.TestCase):
    def test_init(self):
        original_message = "<34>1 2003-10-11T22:14:15.003Z mymachine.example.com su - ID47 - BOM'su root' failed for lonvick on /dev/pts/8"
 
        message = log.Log(original_message)
        self.assertEqual(message.prival, 34)
        self.assertEqual(message.version, 1)
        self.assertEqual(message.date, "2003-10-11T22:14:15.003Z")
        self.assertEqual(message.hostname, "mymachine.example.com")
        self.assertEqual(message.appname, "su")
        self.assertEqual(message.procid, "-")
        self.assertEqual(message.msgid, "ID47")
        self.assertEqual(message.structureddata, "-")
        self.assertEqual(message.msg, "'su root' failed for lonvick on /dev/pts/8")
    def test_init_2(self):
        original_message = "<165>1 2003-08-24T05:14:15.000003-07:00 192.0.2.1 myproc 8710 - - %% It's time to make the do-nuts."
# 
        message = log.Log(original_message)
        self.assertEqual(message.prival, 165)
        self.assertEqual(message.version, 1)
        self.assertEqual(message.date, "2003-08-24T05:14:15.000003-07:00")
        self.assertEqual(message.hostname, "192.0.2.1")
        self.assertEqual(message.appname, "myproc")
        self.assertEqual(message.procid, "8710")
        self.assertEqual(message.msgid, "-")
        self.assertEqual(message.structureddata, "-")
        self.assertEqual(message.msg, "%% It's time to make the do-nuts.")

    def test_init_3(self):
        original_message = '<165>1 2003-10-11T22:14:15.003Z mymachine.example.com evntslog - ID47 [exampleSDID@32473 iut="3" eventSource= "Application" eventID="1011"] BOMAn application event log entry.'
        message = log.Log(original_message)
        self.assertEqual(message.prival, 165)
        self.assertEqual(message.version, 1)
        self.assertEqual(message.date, "2003-10-11T22:14:15.003Z")
        self.assertEqual(message.hostname, "mymachine.example.com")
        self.assertEqual(message.appname, "evntslog")
        self.assertEqual(message.procid, "-")
        self.assertEqual(message.msgid, "ID47")
        self.assertEqual(message.structureddata, '[exampleSDID@32473 iut="3" eventSource= "Application" eventID="1011"]')
        self.assertEqual(message.msg, "An application event log entry.")


    def test_init_4(self):
        original_message = '<165>1 2003-10-11T22:14:15.003Z mymachine.example.com evntslog - ID47 [exampleSDID@32473 iut="3" eventSource="Application" eventID="1011"][examplePriority@32473 class="high"]'
        message = log.Log(original_message)
        self.assertEqual(message.structureddata, '[exampleSDID@32473 iut="3" eventSource="Application" eventID="1011"][examplePriority@32473 class="high"]')


    def test_init_5a(self):
        original_message = '<30>Nov 27 19:48:10 mymachine.example.com evntslog - ID47 [exampleSDID@32473 iut="3" eventSource="Application" eventID="1011"][examplePriority@32473 class="high"]'
        message = log.Log(original_message)
        self.assertEqual(message.version, None)
        self.assertEqual(message.date, "Nov 27 19:48:10")
        
    def test_init_5(self):
        original_message = '<30>Nov 27 19:48:10 lenovo systemd[1]: Stopping System Logging Service...'
        message = log.Log(original_message)
        self.assertEqual(message.appname, "systemd")
        self.assertEqual(message.msg, "Stopping System Logging Service...")

    def test_init_6(self):
        original_message = '<13>1 2022-03-23T21:52:03.510734+01:00 lenovo cm - - [timeQuality tzKnown="1" isSynced="1" syncAccuracy="234000"] xyz'
        message = log.Log(original_message)
        self.assertEqual(message.msg, "xyz")
# In this example, the VERSION is 1 and the Facility has the value of
#   4.  The Severity is 2.  The message was created on 11 October 2003 at
#   10:14:15pm UTC, 3 milliseconds into the next second.  The message
#   originated from a host that identifies itself as
#   "mymachine.example.com".  The APP-NAME is "su" and the PROCID is
#   unknown.  The MSGID is "ID47".  The MSG is "'su root' failed for
#   lonvick...", encoded in UTF-8.  The encoding is defined by the BOM.
#   There is no STRUCTURED-DATA present in the message; this is indicated
#   by "-" in the STRUCTURED-DATA field.

if __name__ == '__main__':
    unittest.main()
