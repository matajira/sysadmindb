from django.http import HttpResponse
from .models import Log
from django.template import loader
import sqlite3
from collections import namedtuple
import random
from datetime import datetime
from itertools import groupby
from subprocess import Popen, PIPE, STDOUT
import os
import pathlib

def log_browser(request):
    if request.method == "POST":
        command = request.POST['command']
        command_result = __execute_query(command)

        log_list = __get_current_list_of_logs()
        barchart_labels, barchart_values = __get_barchart_labels_and_values(log_list)

        template = loader.get_template('log_browser/logs.j2')
        context = {
            'log_list': log_list,
            'barchart_labels': barchart_labels,
            'barchart_values': barchart_values,
            'command_result': command_result,
            'command': command,
        }
        return HttpResponse(template.render(context, request))
    else:
        log_list = __get_current_list_of_logs()
        barchart_labels, barchart_values = __get_barchart_labels_and_values(log_list)
        template = loader.get_template('log_browser/logs.j2')
        context = {
            'log_list': log_list,
            'barchart_labels': barchart_labels,
            'barchart_values': barchart_values,
        }
        return HttpResponse(template.render(context, request))

def __get_current_list_of_logs():
    Log = namedtuple('Log',['prival', 'version' ,'date' ,'hostname' ,'appname' ,'procid' ,'msgid' ,'structureddata' ,'msg' ,'original_msg', 'timestamp'])
    log_list = []

    con = sqlite3.connect('sysadmindb/test.db')
    cur = con.cursor()
    cur.execute("select * from logs")

    for log in cur.fetchall():
        log_list.append(Log(*log))
    return log_list

def __get_barchart_labels_and_values(log_list):
    barchart_values = []
    barchart_labels = []
    get_day = lambda x: datetime.fromtimestamp(x.timestamp).strftime('%Y-%m-%d %H:%M')
    log_list.sort(key=get_day)

    for day, items in groupby(log_list, key=get_day):
        barchart_labels.append(str(day))
        barchart_values.append(len(list(items)))
    return barchart_labels, barchart_values


def __execute_query(command : str) -> str:
    def append_logs(query_result: tuple) -> str:
        result = ""
        original_log_line_position = 9
        for i in query_result:
            result += i[original_log_line_position] + "\n"
        return result

    def execute_users_command(logs: str, users_command: str) -> str:
        my_env = dict()
        my_env["PATH"] = str(pathlib.Path(os.path.realpath(__file__)).parent) + "/restricted_bin"
        p = Popen(["/bin/bash", "--norc", "--noprofile", "--restricted", "-c", users_command], stdout=PIPE, stdin=PIPE, stderr=PIPE, shell=False, env=my_env)
        position_of_stdout, position_of_stderr = 0,1
        process_result = p.communicate(input=logs.encode())
        stdout = process_result[position_of_stdout]
        stderr = process_result[position_of_stderr]
        print(stdout)
        print(stderr)
        return stdout

    def parse_stdout_of_users_command(stdout: str) -> list:
        return stdout.decode("utf-8").split("\n")

    con = sqlite3.connect('sysadmindb/test.db')
    cur = con.cursor()
    cur.execute("select * from logs")
    appended_logs = append_logs(cur.fetchall())
    con.close()

    stdout_users_command = execute_users_command(appended_logs, command)
    return parse_stdout_of_users_command(stdout_users_command)
