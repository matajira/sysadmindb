from django.apps import AppConfig


class LogBrowserConfig(AppConfig):
    name = 'log_browser'
