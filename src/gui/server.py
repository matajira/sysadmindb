from gui.wsgi import application
from waitress import serve

serve(application, listen='*:8080')
