import socketserver
import sqlite3
import pprint
import threading
from sysadmindb.tcp_server import SysadminDBTcpServer


import sys
sys.path.append('./gui')

from gui.gui.wsgi import application
from waitress import serve

def initialize_web_server():
    serve(application, listen='*:8080')

def initialize_tcp_server():
    server = SysadminDBTcpServer()

if __name__=='__main__':
    t1 = threading.Thread(target=initialize_web_server)
    t2 = threading.Thread(target=initialize_tcp_server)
    t1.start()
    t2.start()
